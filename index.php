<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require './email.class.php';

require_once './vendor/autoload.php';
function testEmail($server,$port,$username,$password,$secure){
    $mail = new PHPMailer();
    $emailConfig = new email;
    $emailConfig->host = $server;
    $emailConfig->port = $port;
    $emailConfig->auth = true;
    $emailConfig->secure =$secure;
    $emailConfig->username = $username;
    $emailConfig->password = $password;
    $emailConfig->from = $username;
    $emailConfig->fromName = $username;
    // return print_r($emailConfig);
    $mail->isSMTP();
    $mail->Host = $emailConfig->host;
    $mail->Port = $emailConfig->port;   
    $mail->SMTPAuth = $emailConfig->auth;     // turn on SMTP authentication
    $mail->SMTPSecure = $emailConfig->secure;  
    // $mail->SMTPDebug    = 3; 
    $mail->Username = $emailConfig->username;  // SMTP username
    $mail->Password = $emailConfig->password; // SMTP password
    $mail->From = $emailConfig->from;
    $mail->FromName = $emailConfig->fromName;
    
    
    if($mail->smtpConnect()){
        $mail->smtpClose();
       return true;
    }
    else{
        return false;
    }

}
if(isset($_POST['hostname'])){

    $emails     = $arr = explode("\n",$_POST['emails']); 
    $isSecure   = isset($_POST['secure']) ? $_POST['secure']:'';
    $server     = $_POST['hostname'];
    $port       = $_POST['port'];
    $success    = [];
    foreach ($emails as $key) {
        $key        = explode(':',$key);
        $email      = trim($key[0]);
        $password   = trim($key[1]);
        $test = testEmail($server,$port,$email,$password,$isSecure);
        // echo "<pre>$test</pre>";
        if($test){
            $success[] = "$email:$password";
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="./style.css">
    <title>Checker SMTP</title>
</head>
<body>
    <div class="container flex-main">
    <div class="py-4"></div>
        <h1 class="title">Checker SMTP</h1>
        <div class="py-2"></div>
        <form action="" method="POST" class="row justify-content-center">
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="host_name">HOSTNAME</label>
                    <input type="text" name="hostname" id="host_name" class="form-control form-control-sm">
                </div>
                <div class="form-group">
                    <label for="port">PORT</label>
                    <input type="text" name="port" id="port" class="form-control form-control-sm">
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" name="secure" value="ssl" class="custom-control-input" id="ssl">
                    <label class="custom-control-label"  for="ssl">SSL</label>
                </div>                
                <div class="custom-control custom-radio">
                    <input type="radio" name="secure" value="tls" class="custom-control-input" id="tls">
                    <label class="custom-control-label"  for="tls">TLS</label>
                </div>                
            </div>
            <div class="col-sm-12 col-md-4">
                <label for="list_smtp">List SMTP</label>
                <textarea name="emails" id="emails" class="form-control"></textarea>
                <br/>
               <?php 
                    if(isset($success)){
                        ?>
                        <strong>Valid SMTP</strong>
                        <div class="card text-success" readonly class="form-control">
                            <?php 
                            foreach ($success as $key) {
                               echo $key."<br/>";
                            }?>
                        </textarea>
                        <?php 
                    }
               ?>
            </div>
            <div class="col-sm-12 col-md-6">
            </div>
            <div class="col-sm-12 py-4 text-center">
                <button type="submit" class="btn btn-outline-success">Start Checking</button>
            </div>
        </form>
    </div>
</body>
</html>