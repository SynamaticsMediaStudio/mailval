<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require './email.class.php';

require_once './vendor/autoload.php';

$test = testEmail("smtp.orange.fr","25","titiportes1@orange.fr","jesuisbi27","");

function testEmail($server,$port,$username,$password,$secure){
    $mail = new PHPMailer();
    $emailConfig = new email;
    $emailConfig->host = $server;
    $emailConfig->port = $port;
    $emailConfig->auth = true;
    $emailConfig->secure =$secure;
    $emailConfig->username = $username;
    $emailConfig->password = $password;
    $emailConfig->from = $username;
    $emailConfig->fromName = $username;
    
    $mail->isSMTP();
    $mail->SMTPDebug    = 2; 
    $mail->Host = $emailConfig->host;
    $mail->Port = $emailConfig->port;   
    $mail->SMTPAuth = $emailConfig->auth;     // turn on SMTP authentication
    $mail->SMTPSecure = $emailConfig->secure;  
    $mail->Username = $emailConfig->username;  // SMTP username
    $mail->Password = $emailConfig->password; // SMTP password
    $mail->From = $emailConfig->from;
    $mail->FromName = $emailConfig->fromName;
    
    
    if($mail->smtpConnect()){
        $mail->smtpClose();
       return true;
    }
    else{
        return false;
    }

}
//"myldtwgddnimubox"
?>